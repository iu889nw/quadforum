(function($) {
  "use strict"; // Start of use strict

  // Floating label headings for the contact form
  $("body").on("input propertychange", ".floating-label-form-group", function(e) {
    $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
  }).on("focus", ".floating-label-form-group", function() {
    $(this).addClass("floating-label-form-group-with-focus");
  }).on("blur", ".floating-label-form-group", function() {
    $(this).removeClass("floating-label-form-group-with-focus");
  });

  // Show the navbar when the page is scrolled up
  var MQL = 992;

  //primary navigation slide-in effect
  if ($(window).width() > MQL) {
      var mainNav = $('#mainNav');
      var headerHeight = mainNav.height();

      $(window).on('scroll', {
        previousTop: 0
      },
      function() {
        var currentTop = $(window).scrollTop();
        //check if user is scrolling up
        if (currentTop < this.previousTop) {
          //if scrolling up...
            if (currentTop > 0 && mainNav.hasClass('is-fixed')) {
                mainNav.addClass('is-visible');
          } else {
                mainNav.removeClass('is-visible is-fixed');
          }
        } else if (currentTop > this.previousTop) {
          //if scrolling down...
            mainNav.removeClass('is-visible');
            if (currentTop > headerHeight && !mainNav.hasClass('is-fixed')) mainNav.addClass('is-fixed');
        }
        this.previousTop = currentTop;
      });
  }
    $(document).ready(function () {
        /**
         * BACK TO TOP
         */
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#backToTop').fadeIn();
            } else {
                $('#backToTop').fadeOut();
            }
        });
        $('#backToTop').click(function () {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);
        });

        /**
         * INIT  TOOLTIP
         */
        $('[data-toggle="tooltip"]').tooltip();

        /**
         * INPUT FOR POST COMMENT
         **/
        $('.new').click(function () {
            console.log("clicked");
            $('#comment-area').removeClass('d-none');
            $('.new').hide();
            $('html, body').animate({
                scrollTop: $("#comment-area").offset().top
            }, 1000);
        });
        tinymce.init({
            language: 'sk',
            selector: '#comment-body'
        });

        tinymce.init({
            language: 'sk',
            selector: '#body'
        });

        /**
         * BAZAR NEW ITEM HIDE AND SHOW FORMS ACCORDING TO CATEGORY
        * */
        var categoryButton = $('#category-button');
        $(".menu-img").click(function (e) {
            var id = e.target.id;
            console.log(id);
            var formId = '#' + id + '-form';
            console.log(formId);
            $('#menu').hide();
            $(formId).removeClass('d-none');
            $('#category-button').removeClass('d-none');
        });

        categoryButton.click(function () {
            $('#menu').show();
            categoryButton.addClass('d-none');
            $('.form-horizontal').addClass('d-none');
            $('.board-index').innerHTML += ""
        });

        /**
         * RANGE SLIDERS FOR BAZAR
         * -------------------------------------- **/
        $('#price').ionRangeSlider({
            type: "double",
            step: 50,
            min: 0,
            max: 10000,
            prettify_enabled: true
        });
        $('#power').ionRangeSlider({
            type: "double",
            step: 10,
            min: 0,
            max: 1200,
            prettify_enabled: true

        });
        $('#year').ionRangeSlider({
            type: "double",
            min: 1990,
            max: 2018
        });
        /**
         * BAZAR FILTER SHOW/HIDE
         */
        var filterBtn = $('#filterBtn');

        var closeFilterBtn = $('#closeFilterBtn');
        filterBtn.click(function () {
           filterBtn.addClass('d-none');
           $('#filter').removeClass('d-none');
           closeFilterBtn.removeClass('d-none');

        });
        closeFilterBtn.click(function () {
            filterBtn.removeClass('d-none');
            $('#filter').addClass('d-none');
            closeFilterBtn.addClass('d-none');

        });

        /**
         * BAZAR ITEM VIEW SCRIPT
         */
        var contactBtn = $('#contact-btn');
        contactBtn.click(function () {
            contactBtn.addClass('d-none');
            $('.contact').removeClass('d-none');
        });

        $("#gallery").unitegallery({
            gallery_theme: "compact",
            theme_panel_position: "right"
        });

        var form = document.getElementById('needs-validation');
        console.log("asdf");
        form.addEventListener('submit', function (event) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            }
            form.classList.add('was-validated');
        }, false);

    });


    //cookies information
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#363434"
                },
                "button": {
                    "background": "#aa0000"
                }
            },
            "showLink": false,
            "theme": "edgeless",
            "content": {
                "message": "Táto stránka používa cookies aby sme dokázali zlepšiť váš zážitok.",
                "dismiss": "Rozumiem"
            }
        })});


})(jQuery); // End of use strict
