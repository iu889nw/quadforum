package forum.quadforum.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import java.util.List;

/**
 *
 */
//@Repository
public interface UserRolesRepository extends CrudRepository<UserRole, Long> {

    @Query("select a.role from UserRole a, User b where b.username=?1 and a.userId=b.userId")
    List<String> findRoleByUserName(String username);

    UserRole findOneByUserId(Long userId);

}