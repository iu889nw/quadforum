package forum.quadforum.domain;

import forum.quadforum.domain.bazar.BazarItem;
import forum.quadforum.domain.forum.Comment;
import forum.quadforum.domain.forum.Post;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "username")
    private String username;

    @Column(name = "location")
    private String location;


    @Column(name = "first_name")
    @NotEmpty(message = "Please provide your first name")
    private String firstName;

    @Column(name = "last_name")
    @NotEmpty(message = "Please provide your last name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    @Email(message = "Please provide a valid e-mail")
    @NotEmpty(message = "Please provide an e-mail")
    private String email;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(name = "post_count")
    private Integer postCount;

    @Column(name = "created")
    private Date created;

    @Column(name = "confirmation_token")
    private String confirmationToken;

    @Column(name = "reset_token")
    private String resetToken;

    @Column(name = "role")
    private String role;

    @OneToOne(mappedBy = "user")
    private FileUpload userImg;

    @OneToMany(mappedBy = "user")
    private Set<Post> posts = new HashSet<Post>();

    @OneToMany(mappedBy = "user")
    private Set<Comment> comments = new HashSet<Comment>();

    @OneToMany(mappedBy = "user")
    private Set<BazarItem> bazarItems = new HashSet<BazarItem>();



    public User() {
    }

    public User(User user) {
        if (user != null) {
            this.userId = user.getUserId();
            this.firstName = user.firstName;
            this.lastName = user.lastName;
            this.username = user.username;
            this.password = user.password;
            this.email = user.email;
            this.phone = user.phone;
            this.enabled = user.enabled;
            this.postCount = 0;
            this.created = user.created;
            this.role = user.role;
        }
    }

    public User(String username, String firstName, String lastName, String password, String email,String phone,String role){
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.enabled = true;
        this.postCount = 0;
        this.created = new Date();
        this.role = role;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPostCount() {
        return this.posts.size() + this.comments.size();
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }
    public Set<Post> getPosts() {
        return posts;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<BazarItem> getBazarItems() {
        return bazarItems;
    }

    public void setBazarItems(Set<BazarItem> bazarItems) {
        this.bazarItems = bazarItems;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public FileUpload getUserImg() {
        return userImg;
    }

    public void setUserImg(FileUpload userImg) {
        this.userImg = userImg;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", enabled=" + enabled +
                ", postCount=" + postCount +
                ", created=" + created +
                '}';
    }
}