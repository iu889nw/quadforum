package forum.quadforum.domain;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.*;
import forum.quadforum.domain.*;
/**
 *  filename - the name of the file supplied by the frontend. This will also serve as id to allow only unique filenames
    file - a byte array of the file contents. It is annotated with @Lob, telling the database that it will be of type blob
    mimeType - file format which will assist the browser in suggesting a means of opening the file upon download
*/
@Entity
public class FileUpload {





    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "filename")
    private String filename;

    @Lob
    @Column(name = "file")
    private byte[] file;

    @Column(name = "mime_type")
    private String mimeType;

    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    public FileUpload() {
        // Default Constructor
    }

    public FileUpload(String filename, byte[] file, String mimeType) {

        this.file = file;
        this.filename = filename;
        this.mimeType = mimeType;
    }

    public FileUpload(String filename, byte[] file, String mimeType, User user) {
        this.filename = filename;
        this.file = file;
        this.mimeType = mimeType;
        this.user = user;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String generateBase64Image()
    {
        return Base64.encodeBase64String(this.getFile());
    }
}
