package forum.quadforum.domain.bazar;

import forum.quadforum.domain.User;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * BazarItem class for bazar items
 */
@Entity
@Table(name = "bazar")
public class BazarItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "category")
    private  String category;

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    @Column(name= "subcategory")
    private String subcategory;

    @Column(name = "price")
    private Double price;

    @Column(name ="power")
    private Double power;

    @Column(name = "year")
    private Integer year;

    @Column(name = "state")
    private String state;

    @Column(name = "location")
    private String location;

    @Column(name = "ad_type")
    private String adType;

    @Column(name = "phone")
    private String phone;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "created")
    private Date created;

    @Column(name = "brand")
    private String brand;

    @OneToMany(mappedBy = "bazarItem")
    private Set<BazarImg> images = new HashSet<BazarImg>();

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    /**
     * empty constructor
     */
    public BazarItem() {
    }

    /**
     * constructor with brand - insert new quad
     * @param category - category of bazarItem
     * @param title - title for bazarItem
     * @param text
     * @param subcategory
     * @param brand
     * @param price
     * @param year
     * @param state
     * @param location
     * @param adType
     * @param user
     */
    public BazarItem(String category, String title, String text,String subcategory,Double power,String brand,Double price, Integer year,  String state, String location, String adType, User user) {
        this.category = category;
        this.title = title;
        this.text = text;
        this.subcategory = subcategory;
        this.brand = brand;
        this.price = price;
        this.power = power;
        this.year = year;
        this.state = state;
        this.location = location;
        this.adType = adType;
        this.phone = user.getPhone();
        this.email = user.getEmail();
        this.user = user;
        this.created = new Date();
    }

    /**
     * CONSTRUCTOR WITH USER BUT WITHOUT BRAND (for other then quad category)
     * @param category
     * @param title
     * @param text
     * @param subcategory
     * @param price
     * @param year
     * @param state
     * @param location
     * @param adType
     * @param user

     */
    public BazarItem(String category, String title, String text, String subcategory, Double price, Integer year, String state, String location, String adType, User user) {
        this.category = category;
        this.title = title;
        this.text = text;
        this.subcategory = subcategory;
        this.price = price;
        this.year = year;
        this.state = state;
        this.location = location;
        this.adType = adType;
        this.phone = user.getPhone();
        this.email = user.getEmail();
        this.created = new Date();
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAdType() {
        return adType;
    }

    public void setAdType(String adType) {
        this.adType = adType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public Set<BazarImg> getImages() {
        return images;
    }

    public void setImages(Set<BazarImg> images) {
        this.images = images;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * Returns first image for bazar item list preview
     * @return Base64Image string
     */
    public String getFirstImage(){
        return this.getImages().iterator().next().generateBase64Image();
    }

    public Double getPower() {
        return power;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "BazarItem{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", subcategory='" + subcategory + '\'' +
                ", price=" + price +
                ", year=" + year +
                ", state='" + state + '\'' +
                ", location='" + location + '\'' +
                ", adType='" + adType + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", created=" + created +
                ", brand='" + brand + '\'' +
                '}';
    }
}

