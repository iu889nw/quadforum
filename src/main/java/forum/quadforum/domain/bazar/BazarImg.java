package forum.quadforum.domain.bazar;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.*;

@Entity
@Table(name = "bazar_images")
public class BazarImg {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "filename")
    private String filename;

    @Lob
    @Column(name = "file")
    private byte[] file;

    @Column(name = "mime_type")
    private String mimeType;

    @ManyToOne(fetch = FetchType.LAZY)
    private BazarItem bazarItem;


    public BazarImg() {
    }

    public BazarImg(String filename, byte[] file, String mimeType,BazarItem bazarItem) {
        this.filename = filename;
        this.file = file;
        this.mimeType = mimeType;
        this.bazarItem = bazarItem;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BazarItem getBazarItem() {
        return bazarItem;
    }

    public void setBazarItem(BazarItem bazarItem) {
        this.bazarItem = bazarItem;
    }

    public String generateBase64Image()
    {
        return Base64.encodeBase64String(this.getFile());
    }
}
