package forum.quadforum.domain.forum;

import forum.quadforum.domain.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="id")
    private Long id;

    @Column(name = "body")
    @Lob
    private String body;

    @Column(name = "likes")
    private Integer likes;

    @Column(name = "dislikes")
    private Integer dislikes;

    @Column(nullable = false,name = "created")
    private Date created;

    @ManyToOne( optional = false,fetch = FetchType.EAGER)
    private Post post;

    @ManyToOne( optional = false,fetch = FetchType.EAGER)
    private User user;

    public Comment() {
    }

    public Comment(String body, User user, Post post) {
        this.body = body;
        this.likes = 0;
        this.dislikes = 0;
        this.created = new Date();
        this.post = post;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", body='" + body + '\'' +
                ", likes=" + likes +
                ", dislikes=" + dislikes +
                ", created=" + created +
                '}';
    }
}
