package forum.quadforum.domain.forum;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name ="id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "newest_post")
    private Long newestPost;

    @Column(name = "topPost")
    private Long topPost;

    @OneToMany(mappedBy = "category")
    private Set<Post> posts = new HashSet<Post>();

    public Category() {
    }

    public Category(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Post getNewestPost() {
        for (Post p: this.posts){
            if (p.getId() == this.newestPost){
                return p;
            }
        } return  null;
    }

    public void setNewestPost(Long newestPost) {
        this.newestPost = newestPost;
    }

    public Post getTopPost() {
        for (Post p: this.posts){
            if (p.getId() == this.topPost){
                return p;
            }
        }
        return null ;
    }

    public void setTopPost(Long topPost) {
        this.topPost = topPost;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", newestPost=" + newestPost +
                ", topPost=" + topPost +
                '}';
    }
}
