package forum.quadforum.domain.forum;

import forum.quadforum.domain.User;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "posts")
public class Post {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="title")
    private String title;

    @Column(name = "body")
    @Lob
    private String body;

    @Column(name = "replies")
    private Integer replies;

    @Column(name = "views")
    private Integer views;

    @ManyToOne( optional = false,fetch = FetchType.EAGER)
    private Category category;

    @ManyToOne( optional = false,fetch = FetchType.EAGER)
    private User user;

    @OneToMany(mappedBy = "post")
    private Set<Comment> comments = new HashSet<Comment>();

    @OneToMany(mappedBy = "post")
    private Set<Instruction> instructions = new HashSet<Instruction>();

    @Column(name = "likes")
    private Integer likes;

    @Column(name = "dislikes")
    private Integer dislikes;

    @Column(nullable = false,name = "created")
    private Date created;


    public Post() {
    }

    public Post(String title, String body, Category category, User user) {
        this.title = title;
        this.body = body;
        this.category = category;
        this.user = user;
        this.likes =0;
        this.dislikes = 0;
        this.views = 0;
        this.replies = 0;
        this.created = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getReplies() {
        return this.comments.size();
    }

    public void setReplies(Integer replies) {
        this.replies = replies;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Comment getLastReply(){

        List<Comment> commentList =  new ArrayList<>(this.comments);
        if(commentList.size() != 0)
            return commentList.get(commentList.size() -1);
        else return null;
    }

    public void addViewsCount() {
        this.views++;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", user=" + user +
                ", likes=" + likes +
                ", dislikes=" + dislikes +
                ", created=" + created +
                '}';
    }
}
