package forum.quadforum.domain.forum;

import javax.persistence.*;


/**
 * filename - the name of the file supplied by the frontend. This will also serve as id to allow only unique filenames
 * file - a byte array of the file contents. It is annotated with @Lob, telling the database that it will be of type blob
 * mimeType - file format which will assist the browser in suggesting a means of opening the file upon download
 */
@Entity
@Table(name = "instructions")
public class Instruction {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "filename")
    private String filename;

    @Lob
    @Column(name = "file")
    private byte[] file;

    @Column(name = "mime_type")
    private String mimeType;


    @ManyToOne(fetch = FetchType.LAZY)
    private Post post;

    public Instruction() {
        // Default Constructor
    }

    public Instruction(String filename, byte[] file, String mimeType, Post post) {
        this.file = file;
        this.filename = filename;
        this.mimeType = mimeType;
        this.post = post;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "Instruction{" +
                "id=" + id +
                ", filename='" + filename + '\'' +
                ", mimeType='" + mimeType + '\'' +
                '}';
    }
}
