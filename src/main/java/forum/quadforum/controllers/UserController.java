package forum.quadforum.controllers;

import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;
import forum.quadforum.domain.*;
import forum.quadforum.repos.FileUploadRepository;
import forum.quadforum.services.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

@Controller
public class UserController {


    @Autowired
    EmailService emailService;
    @Autowired
    UserRepository userRepo;
    @Autowired
    UserRolesRepository userRolesRepository;
    @Autowired
    FileUploadRepository fileUploadRepo;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final Logger log = LoggerFactory.getLogger(DefaultController.class);

    /**
     * Return register view
     *
     * @param modelAndView
     * @param user
     * @return
     */
    @RequestMapping(value="/register", method = RequestMethod.GET)
    public ModelAndView showRegistrationPage(ModelAndView modelAndView, User user){
        modelAndView.addObject("user", user);
        modelAndView.setViewName("usermanagement/register");
        return modelAndView;
    }

    /**
     * Register new user with created token
     * @param modelAndView
     * @param user
     * @param bindingResult
     * @param request
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView processRegistrationForm(ModelAndView modelAndView, @Valid User user, BindingResult bindingResult, HttpServletRequest request) {


        // Lookup user in database by e-mail

        User userExists = userRepo.findByEmail(user.getEmail());

        if (userExists != null) {
            log.info(userExists.toString());
            modelAndView.addObject("alreadyRegisteredMessage", "Oops!  Používateľ s týmto emailom už existuje..");
            modelAndView.setViewName("usermanagement/register");
            bindingResult.reject("email");
        }
        if (user.getUsername().equals(user.getFirstName())) {
            log.info("Attack");
            modelAndView.addObject("sameUsername", "Používateľské meno (prezývka) a prvé meno sa musia odlišovať..");
            modelAndView.setViewName("usermanagement/register");
            bindingResult.reject("user");
        }

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("usermanagement/register");
        } else { // new user so we create user and send confirmation e-mail

            // Disable user until they click on confirmation link in email
            user.setEnabled(false);
            user.setCreated(new Date());
            user.setRole("user");

            // Generate random 36-character string token for confirmation link
            user.setConfirmationToken(UUID.randomUUID().toString());
            userRepo.save(user);
            userRolesRepository.save(new UserRole(user.getUserId(),"ROLE_USER"));

            String appUrl = request.getScheme() + "://" + request.getServerName();

            SimpleMailMessage registrationEmail = new SimpleMailMessage();
            registrationEmail.setTo(user.getEmail());
            registrationEmail.setSubject("Registration Confirmation");
            registrationEmail.setText("Pre potvrdenie Vašej emailovej adresy kliknite na nižšie uvedený link:\n"
                    + appUrl + "/confirm?token=" + user.getConfirmationToken());
            registrationEmail.setFrom("noreply@domain.com");

            emailService.sendEmail(registrationEmail);

            modelAndView.addObject("confirmationMessage", "Email pre potvrdenie registrácie bol odoslaný na email:" + user.getEmail());
            modelAndView.setViewName("usermanagement/register");
        }

        return modelAndView;
    }

    /**
     * Confirmation page
     * @param modelAndView
     * @param token
     * @return
     */
    @RequestMapping(value="/confirm", method = RequestMethod.GET)
    public ModelAndView showConfirmationPage(ModelAndView modelAndView, @RequestParam("token") String token) {

        User user = userRepo.findByConfirmationToken(token);

        if (user == null) { // No token found in DB
            modelAndView.addObject("invalidToken", "Oops!  This is an invalid confirmation link.");
        } else { // Token found
            modelAndView.addObject("confirmationToken", user.getConfirmationToken());

        }

        modelAndView.setViewName("usermanagement/confirm");
        return modelAndView;
    }

    /**
     * Process confirmation link - user password creation + password strength check
     * @param modelAndView
     * @param bindingResult
     * @param requestParams
     * @param redir
     * @return
     */
    @RequestMapping(value="/confirm", method = RequestMethod.POST)
    public ModelAndView processConfirmationForm(ModelAndView modelAndView, BindingResult bindingResult, @RequestParam Map requestParams, RedirectAttributes redir) {

        modelAndView.setViewName("usermanagement/confirm");

        Zxcvbn passwordCheck = new Zxcvbn();

        Strength strength = passwordCheck.measure(requestParams.get("password").toString());
        // Find the user associated with the reset token
        User user = userRepo.findByConfirmationToken(requestParams.get("token").toString());


        // Set new password
        if (requestParams.get("password").equals(requestParams.get("ConfirmPassword"))) {

            if (strength.getScore() < 2) {
                bindingResult.reject("password");

                redir.addFlashAttribute("errorMessage", "Heslo je príliš jednoduché.");

                modelAndView.setViewName("redirect:usermanagement/confirm?token=" + requestParams.get("token"));
                log.info(requestParams.get("token").toString());
                return modelAndView;
            }

            user.setPassword(passwordEncoder.encode(requestParams.get("password").toString()));
            // Set user to enabled
            user.setEnabled(true);
            // Save user
            userRepo.save(user);
            modelAndView.addObject("successMessage", "Vaše heslo bolo úspešne nastavené!");
            return modelAndView;

        } else {
            bindingResult.reject("password");
            redir.addFlashAttribute("errorMessage", "Heslá niesú zhodné!");
            modelAndView.setViewName("redirect:usermanagement/confirm?token=" + requestParams.get("token"));
            log.info(requestParams.get("token").toString());
            return modelAndView;
        }

    }

    /**
     * Edit user info - POST request from profile view
     * @param email
     * @param phone
     * @param request
     * @return
     */
    @RequestMapping(value = "/uploadWithData", method = RequestMethod.POST)
    public ModelAndView uploadFileWithData(@RequestParam("email") String email,
                                           @RequestParam("phone") String phone,
                                           MultipartHttpServletRequest request ) {
        User user;
        try {
            user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            log.info("Editing user" + user.toString());
            log.info("User edited" + user.toString());
            Iterator<String> itr = request.getFileNames();
            while (itr.hasNext()) {
                String uploadedFile = itr.next();
                MultipartFile file = request.getFile(uploadedFile);
                String mimeType = file.getContentType();
                String filename = file.getOriginalFilename();
                byte[] bytes = file.getBytes();

                FileUpload newFile;
                FileUpload oldFile = fileUploadRepo.findByUser(user);
                if(oldFile != null){
                    log.info("user already have profile image");
                    fileUploadRepo.delete(oldFile);
                    newFile = new FileUpload(filename, bytes, mimeType,user);
                    fileUploadRepo.save(newFile);
                } else {
                    log.info("user dont have profile image");
                    newFile = new FileUpload(filename, bytes, mimeType,user);
                    fileUploadRepo.save(newFile);
                }
                editUser(user,phone,email,newFile);

            }
            return new ModelAndView("redirect:/profile");
        } catch (Exception e) {
            log.info(e.toString());
            return new ModelAndView("error");
        }

    }


    /**
     * Edit user data - called from method above
     * @param user
     * @param phone
     * @param email
     * @param img
     */
    private void editUser(User user, String phone, String email, FileUpload img){
        User editedUser = new User(user);
        editedUser.setEmail(email);
        editedUser.setPhone(phone);
        editedUser.setUserImg(img);
        editedUser.setCreated(user.getCreated());
        userRepo.save(editedUser);
    }

    /**
     * Profile image view
     * @param model
     * @return
     */
    @RequestMapping(value = "/profile",method = RequestMethod.GET)
    public String profile(Model model){

        User user;
        FileUpload profileImg;
        String encodedProfileImg;
        try{
            user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            model.addAttribute("user", user);
            log.info(user.toString());
            String role = userRolesRepository.findRoleByUserName(user.getUsername()).get(0);
            if(role.equals("ROLE_ADMIN"))
                role = "admin";
            else role = "user";
            model.addAttribute("userRole",role);
            profileImg = fileUploadRepo.findByUser(user);
            encodedProfileImg = profileImg.generateBase64Image();
            model.addAttribute("profileImg",encodedProfileImg);
        } catch (Exception e){
            log.info(e.toString());

        }
        return "tabs/main/profile";
    }

    /**
     * *****************************************
     * FORGOT PASSWORD
     * *****************************************
     */

    /**
     * Show forgot password template
     *
     * @return
     */
    @RequestMapping(value = "/forgot", method = RequestMethod.GET)
    public ModelAndView displayForgotPasswordPage() {
        return new ModelAndView("usermanagement/forgot");
    }


    /**
     * Process form submission from forgotPassword page
     */
    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    public ModelAndView processForgotPasswordForm(ModelAndView modelAndView, @RequestParam("email") String userEmail, HttpServletRequest request) {

        // Lookup user in database by e-mail
        User user = userRepo.findByEmail(userEmail);

        if (user == null) {
            modelAndView.addObject("errorMessage", "Používateľ s touto emailovou adresou neexistuje");
        } else {
            // Generate random 36-character string token for reset password
            user.setResetToken(UUID.randomUUID().toString());
            // Save token to database
            userRepo.save(user);
            String appUrl = request.getScheme() + "://" + request.getServerName();
            // Email message
            SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
            passwordResetEmail.setFrom(" quadforum.info@gmail.com");
            passwordResetEmail.setTo(user.getEmail());
            passwordResetEmail.setSubject("Password Reset Request");
            passwordResetEmail.setText("Pre reset hesla kliknite na link nižšie:\n" + appUrl
                    + "/reset?token=" + user.getResetToken());
            emailService.sendEmail(passwordResetEmail);
            // Add success message to view
            modelAndView.addObject("successMessage", "Link pre reset hesla bol odoslaný na email: " + userEmail);
        }
        modelAndView.setViewName("usermanagement/forgot");
        return modelAndView;

    }

    // Display form to reset password
    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public ModelAndView displayResetPasswordPage(ModelAndView modelAndView, @RequestParam("token") String token) {

        User user = userRepo.findByResetToken(token);
        if (user != null) { // Token found in DB
            modelAndView.addObject("resetToken", token);
        } else { // Token not found in DB
            modelAndView.addObject("errorMessage", "Oops!  Toto je nesprávny link pre reset hesla.");
        }
        modelAndView.setViewName("usermanagement/reset");
        return modelAndView;
    }

    // Process reset password form
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ModelAndView setNewPassword(ModelAndView modelAndView, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {

        // Find the user associated with the reset token
        User resetUser = userRepo.findByResetToken(requestParams.get("token"));

        // This should always be non-null but we check just in case
        if (resetUser != null) {

            // Set new password
            resetUser.setPassword(passwordEncoder.encode(requestParams.get("password")));
            // Set the reset token to null so it cannot be used again
            resetUser.setResetToken(null);
            // Save user
            userRepo.save(resetUser);
            // In order to set a model attribute on a redirect, we must use
            // RedirectAttributes
            redir.addFlashAttribute("successMessage", "Vaše heslo bolo úspešne zmenené, teraz sa môžete prihlásiť s novým heslom.");
            modelAndView.setViewName("redirect:login");
            return modelAndView;
        } else {
            modelAndView.addObject("errorMessage", "Oops!  Toto nieje správny link pre zmenu hesla.");
            modelAndView.setViewName("usermanagement/reset");
        }

        return modelAndView;
    }

    // Going to reset page without a token redirects to login page
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ModelAndView handleMissingParams(MissingServletRequestParameterException ex) {
        return new ModelAndView("redirect:login");
    }



}
