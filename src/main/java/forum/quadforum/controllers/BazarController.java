package forum.quadforum.controllers;

import forum.quadforum.domain.User;
import forum.quadforum.domain.bazar.BazarImg;
import forum.quadforum.domain.bazar.BazarItem;
import forum.quadforum.repos.bazar.BazarImgRepository;
import forum.quadforum.repos.bazar.BazarItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@Controller()
@RequestMapping("/bazar")
public class BazarController {

    private static final Logger log = LoggerFactory.getLogger(DefaultController.class);

    @Autowired
    BazarItemRepository bazarItemRepo;

    @Autowired
    BazarImgRepository bazarImgRepo;

    /**
     *
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String bazar() {
        log.info("bazar");
        return "tabs/main/bazar";
    }

    /**
     *
     * @return
     */
    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newItem() {
        log.info("new");
        return "tabs/bazar/new";
    }

    /**
     *
     * @param title
     * @param text
     * @param brand
     * @param subcategory
     * @param price
     * @param year
     * @param state
     * @param location
     * @param adType
     * @param files
     * @return
     */
    @RequestMapping(value = "/createQuad",method = RequestMethod.POST)
    public ModelAndView createQuad(@RequestParam("title-quad") String title, @RequestParam("text-quad") String text,
                                   @RequestParam("brand") String brand, @RequestParam("subcategory-quad") String subcategory,
                                   @RequestParam("price-quad") Double price, @RequestParam("year-quad") Integer year,
                                   @RequestParam("state") String state, @RequestParam("location-quad") String location,
                                   @RequestParam("adType-quad") String adType, @RequestParam("power") Double power,
                                   @RequestParam("images-quad")MultipartFile[] files){

      return createNewBazarItem("quads",title,text,subcategory,price,power,year,state,location,brand,adType,files);
    }

    /**
     *
     * @param title
     * @param text
     * @param subcategory
     * @param price
     * @param year
     * @param state
     * @param location
     * @param adType
     * @param files
     * @return
     */
    @RequestMapping(value = "/createSpare",method = RequestMethod.POST)
    public ModelAndView createSparePart(@RequestParam("title-part") String title, @RequestParam("text-part") String text,
                                        @RequestParam("subcategory-part") String subcategory, @RequestParam("price-part") Double price,
                                        @RequestParam("year-part") Integer year, @RequestParam("state-part") String state,
                                        @RequestParam("location-part") String location, @RequestParam("adType-part") String adType,
                                        @RequestParam("images-part") MultipartFile[] files){
        log.info("create spare-part");
        return createNewBazarItem("spare-parts",title,text,subcategory,price,null,year,state,location,null,adType,files);
    }

    /**
     *
     * @param title
     * @param text
     * @param subcategory
     * @param price
     * @param year
     * @param state
     * @param location
     * @param adType
     * @param files
     * @return
     */
    @RequestMapping(value = "/createAcc",method = RequestMethod.POST)
    public ModelAndView createAcc(@RequestParam("title-acc") String title, @RequestParam("text-acc") String text,
                                  @RequestParam("subcategory-acc") String subcategory, @RequestParam("price-acc") Double price,
                                  @RequestParam("year-acc") Integer year, @RequestParam("state-acc") String state,
                                  @RequestParam("location-acc") String location, @RequestParam("adType-acc") String adType,
                                  @RequestParam("images-acc") MultipartFile[] files){
        log.info("create accessories");
        return createNewBazarItem("accessories",title,text,subcategory,price,null,year,state,location,null,adType,files);

    }

    /**
     *
     * @param title
     * @param text
     * @param subcategory
     * @param price
     * @param year
     * @param state
     * @param location
     * @param adType
     * @param files
     * @return
     */
    @RequestMapping(value = "/createClothing",method = RequestMethod.POST)
    public ModelAndView createClothing(@RequestParam("title-clothing") String title, @RequestParam("text-clothing") String text,
                                       @RequestParam("subcategory-clothing") String subcategory, @RequestParam("price-clothing") Double price,
                                       @RequestParam("year-clothing") Integer year, @RequestParam("state-clothing") String state,
                                       @RequestParam("location-clothing") String location, @RequestParam("adType-clothing") String adType,
                                       @RequestParam("images-clothing") MultipartFile[] files){
        log.info("create clothing");
        return createNewBazarItem("clothing",title,text,subcategory,price,null,year,state,location,null,adType,files);
    }

    /**
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    public String getItem(Model model, @PathVariable("id") Long id){
        BazarItem bazarItem = bazarItemRepo.findOne(id);
        model.addAttribute("bazarItem", bazarItem);
        model.addAttribute("images",bazarItem.getImages());
        log.info(bazarItem.toString());
        return "tabs/bazar/item";
    }

    /**
     *
     * @param model
     * @param category
     * @return
     */
    @RequestMapping(value = "/{category}", method = RequestMethod.GET)
    public String bazarQuads(Model model,@PathVariable("category") String category) {
        log.info(category);
        List<BazarItem> items = bazarItemRepo.findByCategory(category);
        model.addAttribute("items",items);
        return "tabs/bazar/" + category;
    }

    /**
     *
     * @param category
     * @param title
     * @param text
     * @param subcategory
     * @param price
     * @param year
     * @param state
     * @param location
     * @param brand
     * @param adType
     * @param files
     * @return
     */
    private ModelAndView createNewBazarItem(String category,String title,String text,String subcategory,Double price,Double power,
                                           Integer year,String state,String location,String brand,String adType,MultipartFile[] files){
        User user;
        BazarItem bazarItem;
        try{
            user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (brand != null)
                bazarItem = new BazarItem(category,title,text,subcategory,power,brand,price,year,state,location,adType,user);
            else
                bazarItem = new BazarItem(category,title,text,subcategory,price,year,state,location,adType,user);
            bazarItemRepo.save(bazarItem);
            for (MultipartFile file: files) {
                UploadFile(bazarItem, file);
            }
            log.info("quad " + bazarItem.toString() + " created!");
            return new ModelAndView("redirect:/bazar/item/" + bazarItem.getId() );
        } catch (Exception e){
            log.info(e.toString());
            return new ModelAndView("redirect:/bazar/new");
        }
    }

    private void UploadFile(BazarItem bazarItem, MultipartFile file) throws IOException {
        String mimeType = file.getContentType();
        String filename = file.getOriginalFilename();
        byte[] bytes = file.getBytes();
        bazarImgRepo.save(new BazarImg(filename, bytes, mimeType, bazarItem));
    }
}
