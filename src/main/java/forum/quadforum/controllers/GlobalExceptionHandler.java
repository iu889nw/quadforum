package forum.quadforum.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.IOError;
import java.io.IOException;
import java.sql.SQLException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(SQLException.class)
    public String handleSQLException(HttpServletRequest request, Exception ex){
        logger.info("SQLException Occured::URL=" + request.getRequestURL());
        return "error";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException")
    @ExceptionHandler(IOException.class)
    public String handleIOException(){
        logger.error("IOException handler executed");
        return "error";

    }
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "IOError")
    @ExceptionHandler(IOError.class)
    public  String HandleIOException(){
        logger.error("IOError handler executed");
        return "error";
    }

}
