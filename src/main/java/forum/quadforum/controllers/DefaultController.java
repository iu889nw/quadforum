package forum.quadforum.controllers;

import forum.quadforum.domain.UserRepository;
import forum.quadforum.domain.UserRolesRepository;
import forum.quadforum.repos.FileUploadRepository;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;


@Controller
public class DefaultController {

    @Autowired
    FileUploadRepository fileUploadRepo;

    @Autowired
    UserRepository userRepo;

    @Autowired
    UserRolesRepository userRoleRepo;

    private static final Logger log = LoggerFactory.getLogger(DefaultController.class);

    @RequestMapping(value = {"/","/index"}, method = RequestMethod.GET)
    public String index() {
        log.debug("index");
        return "tabs/main/main";
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String news() {
        log.debug("news");
        return "tabs/main/news";
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String error403() {
        return "403";
    }

    @RequestMapping("/login")
    public String login() {
        log.debug("login");
        return "login";
    }

    @RequestMapping(value ="/logout", method = RequestMethod.GET)
    public String logout() {
        log.debug("logout");
        SecurityContextHolder.clearContext();
        return "login";
    }

    @RequestMapping(value = {"/robots", "/robot", "/robots.txt", "/robots.txt"})
    public void robot(HttpServletResponse response) {

        InputStream resourceAsStream = null;
        try {

            ClassLoader classLoader = getClass().getClassLoader();
            resourceAsStream = classLoader.getResourceAsStream("robots.txt");
            response.addHeader("Content-disposition", "filename=robots.txt");
            response.setContentType("text/plain");
            IOUtils.copy(resourceAsStream, response.getOutputStream());
            response.flushBuffer();
        } catch (Exception e) {
            log.error("Problem with displaying robot.txt", e);
        }
    }






}