package forum.quadforum.controllers;

import forum.quadforum.domain.User;
import forum.quadforum.domain.UserRepository;
import forum.quadforum.domain.forum.Category;
import forum.quadforum.domain.forum.Comment;
import forum.quadforum.domain.forum.Instruction;
import forum.quadforum.domain.forum.Post;
import forum.quadforum.repos.InstructionRepository;
import forum.quadforum.repos.forum.CategoryRepository;
import forum.quadforum.repos.forum.CommentRepository;
import forum.quadforum.repos.forum.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
public class ForumController {

    private static final Logger log = LoggerFactory.getLogger(DefaultController.class);

    @Autowired
    private CategoryRepository categoryRepo;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private InstructionRepository instructionRepo;

    @Autowired
    private UserRepository userRepository;


    /**
     * View for forum tab, show all the categories and related newest and top posts
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/forum", method = RequestMethod.GET)
    public String forum(Model model) {
        log.debug("forum");
        List<Category> categoryList = categoryRepo.findAll();
        model.addAttribute("categories",categoryList);
        model.addAttribute("topPosts", postRepository.findTop5ByOrderByCreatedDesc());

        try {
            for (Category c: categoryList) {
                if(postRepository.findAllByCategory(c).size() != 0){
                    c.setNewestPost(postRepository.findFirstByCategoryOrderByCreatedDesc(c).getId());
                    c.setTopPost(postRepository.findFirstByCategoryOrderByViewsDesc(c).getId());
                    categoryRepo.save(c);
                }
            }
            return "tabs/main/forum";
        } catch (Exception e){
            log.info(e.toString());
        }
        return "tabs/main/forum";
    }

    /**
     * View for specific category
     * @param model
     * @param session
     * @param name
     * @return
     */
    @RequestMapping(value = "/category/{name}", method = RequestMethod.GET)
    public String category(Model model, HttpSession session,
                           @PathVariable ("name") String name) {

        log.debug("category: " + name);
        session.setAttribute("category", name);
        Category category = categoryRepo.findByName(name);
        model.addAttribute("category",category);
        model.addAttribute("posts",postRepository.findAllByCategory(category));
        model.addAttribute("topPosts", postRepository.findTop5ByOrderByCreatedDesc());
        return "tabs/forum/category";
    }

    /**
     * Specific post view
     * @param session
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/post/{id}", method = RequestMethod.GET)
    public String post(HttpSession session, Model model, @PathVariable("id") Long id) {

        Post post = postRepository.findOne(id);
        Category category = post.getCategory();
        List<Comment> comments = commentRepo.findAllByPost(post);

        post.addViewsCount();
        postRepository.save(post);

        List<Instruction> instructions = instructionRepo.findAllByPost(post);
        if (!instructions.isEmpty())
            model.addAttribute("instructions", instructions);

        model.addAttribute("post",post);
        session.setAttribute("postId",id);
        model.addAttribute("category",category);
        model.addAttribute("comments", comments);


        log.debug("post: " + id + "category: " + "comments: " + comments.size());
        return "tabs/forum/post";
    }

    /**
     * New post creation with title and body
     * @param session
     * @return
     */
    @RequestMapping(value = "/new-post", method = RequestMethod.GET)
    public String newTopic(HttpSession session, Model model) {
        String category = session.getAttribute("category").toString();
        log.info("new post in: " + category);
        model.addAttribute("category", category);
        return "tabs/forum/new-post";
    }

    /**
     * @param session
     * @param title
     * @param body
     * @return
     */
    @RequestMapping(value = "/new-post", method = RequestMethod.POST)
    public ModelAndView createTopic(HttpSession session,
                                    @RequestParam("title") String title,
                                    @RequestParam("body") String body){
        Category category = categoryRepo.findByName(session.getAttribute("category").toString());
        log.info(category.toString());
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Post post = new Post(title,body,category, user);
        user.setPostCount(user.getPostCount()+1);

        postRepository.save(post);

        return new ModelAndView("redirect:/post/" + post.getId());
    }

    @RequestMapping(value = "/edit-post/{id}", method = RequestMethod.GET)
    public ModelAndView editPost(HttpSession session, Model model,
                                 @PathVariable("id") Long id) {
        Category category = categoryRepo.findByName(session.getAttribute("category").toString());
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Post post = postRepository.findOne(id);
        model.addAttribute("title", post.getTitle());
        model.addAttribute("body", post.getBody());
        return new ModelAndView("redirect:/post/" + post.getId());
    }


    @RequestMapping(value = "/new-post-pdf", method = RequestMethod.POST)
    public ModelAndView createTopicWithInstruction(HttpSession session, @RequestParam("title") String title,
                                                   @RequestParam("body") String body, @RequestParam("instructions") MultipartFile[] files) {
        Category category = categoryRepo.findByName(session.getAttribute("category").toString());
        log.info(category.toString());
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Post post = new Post(title, body, category, user);
        user.setPostCount(user.getPostCount() + 1);
        postRepository.save(post);
        try {
            for (MultipartFile file : files) {
                UploadFile(file, post);
                return new ModelAndView("redirect:/post/" + post.getId());
            }
        } catch (Exception e) {
            log.info(e.toString());
        }
        return new ModelAndView("redirect:/post/" + post.getId());
    }


    @RequestMapping(value = "/new-comment",method = RequestMethod.POST)
    public ModelAndView createComment(HttpSession session, Model model,
                                @RequestParam("comment-body") String body){
        Post post = postRepository.findOne((Long)session.getAttribute("postId"));
        User user =(User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Comment comment  = new Comment(body,user,post);
        commentRepo.save(comment);
        model.addAttribute("post",post);
        model.addAttribute("comments", commentRepo.findAllByPost(post));
        return new ModelAndView("redirect:/post/" + post.getId());
    }

    @RequestMapping(value = "/getPDF/{id}", method = RequestMethod.GET)
    private void downloadPDF(@PathVariable("id") Long id, HttpServletResponse response) {
        Instruction instruction = instructionRepo.findOne(id);
        try {
            response.setHeader("Content-Disposition", "inline;filename=\"" + instruction.getFilename() + "\"");
            response.setContentLength(instruction.getFile().length);
            response.setContentType("application/pdf");
            FileCopyUtils.copy(instruction.getFile(), response.getOutputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void UploadFile(MultipartFile file, Post post) throws IOException {
        String mimeType = file.getContentType();
        String filename = file.getOriginalFilename();
        byte[] bytes = file.getBytes();
        Instruction instruction = new Instruction(filename, bytes, mimeType, post);
        instructionRepo.save(instruction);
        log.info(instruction.toString() + " saved!");
    }

}
