package forum.quadforum.repos.forum;

import forum.quadforum.domain.forum.Category;
import forum.quadforum.domain.forum.Post;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepository extends CrudRepository<Post,Long> {

    List<Post> findAllByCategory(Category category);

    /**
     * Query for newest post in category
     * @param category
     * @return newest post
     */
    Post findFirstByCategoryOrderByCreatedDesc(Category category);

    /**
     * Query for the most viewed post in category
     * @param category
     * @return
     */
    Post findFirstByCategoryOrderByViewsDesc(Category category);

    List<Post> findTop5ByOrderByCreatedDesc();

    List<Post> findTop5ByCategoryOrderByCreatedDesc(Category category);
}
