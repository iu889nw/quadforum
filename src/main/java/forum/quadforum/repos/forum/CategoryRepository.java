package forum.quadforum.repos.forum;

import forum.quadforum.domain.forum.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category,Long> {

    List<Category> findAll();
    Category findByName(String string);
}
