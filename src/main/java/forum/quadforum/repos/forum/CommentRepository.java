package forum.quadforum.repos.forum;

import forum.quadforum.domain.forum.Comment;
import forum.quadforum.domain.forum.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment,Long> {

    List<Comment> findAllByPost(Post post);

}
