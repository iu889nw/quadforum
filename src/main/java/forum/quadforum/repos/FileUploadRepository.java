package forum.quadforum.repos;

import forum.quadforum.domain.FileUpload;
import forum.quadforum.domain.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileUploadRepository extends CrudRepository<FileUpload,Long> {

    FileUpload findByFilename(String filename);
    List<FileUpload> findAll();
    /**
     * user has photo
     * */
    FileUpload findByUser(User user);

}
