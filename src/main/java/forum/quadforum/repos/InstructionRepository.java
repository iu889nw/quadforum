package forum.quadforum.repos;

import forum.quadforum.domain.forum.Instruction;
import forum.quadforum.domain.forum.Post;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InstructionRepository extends CrudRepository<Instruction, Long> {

    List<Instruction> findAll();

    List<Instruction> findAllByPost(Post post);

    Instruction findOne(Long id);
}
