package forum.quadforum.repos.bazar;

import forum.quadforum.domain.bazar.BazarItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BazarItemRepository extends CrudRepository<BazarItem,Long> {

    List<BazarItem> findByCategory(String category);

}
