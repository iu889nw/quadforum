package forum.quadforum.repos.bazar;

import forum.quadforum.domain.bazar.BazarImg;
import forum.quadforum.domain.bazar.BazarItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BazarImgRepository extends CrudRepository<BazarImg,Long> {

    BazarImg findByFilename(String filename);
    List<BazarImg> findAllByBazarItem(BazarItem item);

}

